import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { startTheGame } from '../../actions'
import { withRouter } from 'react-router-dom'

class Home extends PureComponent {
    startTheGame = (type) => {
        this.props.startTheGame(type)
        this.props.history.push('/game')
    }

    render() {
        return (
            <div className="container buttons-wrapper">
                <button type="button" className="btn btn-blue" onClick={() => { this.startTheGame('pvc') }}>Player vs Computer</button>
                {/* <button type="button" className="btn btn-green" onClick={() => { this.startTheGame('cvc') }}>Computer vs Computer</button> */}
            </div>
        )
    }
}

function mapStateToProps (state) {
    return { gameStarted: state.game.started }
}

export default withRouter(connect(mapStateToProps, { startTheGame })(Home))
