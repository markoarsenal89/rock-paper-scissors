import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

class ScoreHistory extends PureComponent {
    render() {
        if (this.props.history.length === 0) {
            return <p>No history yet <span role="img" aria-label="crying face emoji">😥</span></p>
        }

        return (
            <div className="game-history container">
                <ul className="history-score">
                    <li>You: <span className="score-number">{this.props.score.player}</span></li>
                    <li>Computer: <span className="score-number">{this.props.score.computer}</span></li>
                </ul>
                <ul className="history-list">
                    {this.props.history.map((score, i) => {
                        return <li key={i}>
                            <span className={score.winner === 'player' ? 'round-winner' : ''}>You: <i>{score.player.choiceName}</i></span>
                            <span className={score.winner === 'computer' ? 'round-winner' : ''}>Computer: <i>{score.computer.choiceName}</i></span>
                            {score.winner === 'player' ? <strong>You win <span role="img" aria-label="smiling face with open mouth emoji">😃</span></strong> : null}
                            {score.winner === 'none' ? <span>Nobody win <span role="img" aria-label="unamused face emoji">😒</span></span> : null}
                            {score.winner === 'computer' ? <strong>Computer win <span role="img" aria-label="angry face emoji">😠</span></strong> : null}
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

ScoreHistory.propTypes = {
    score: PropTypes.object,
    history: PropTypes.array,
}

ScoreHistory.defaultProps = {
    score: {},
    history: [],
}

function mapStateToProps (state) {
    return {
        score: state.score,
        history: state.history,
    }
}

export default connect(mapStateToProps)(ScoreHistory)
