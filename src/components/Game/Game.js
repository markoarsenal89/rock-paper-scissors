import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ContenderContainer from '../Contender/ContenderContainer'
import celebration from '../../assets/images/celebration.gif'

export default class Game extends PureComponent {
  renderPlayerButtons = () => {
    return (
      this.props.roundFinished === false ?
        <ul className="container player-options">
          <li><button type="button" className="btn btn-sm btn-blue" data-move="0" onClick={this.props.playMove}>Rock</button></li>
          <li><button type="button" className="btn btn-sm btn-blue" data-move="1" onClick={this.props.playMove}>Paper</button></li>
          <li><button type="button" className="btn btn-sm btn-blue" data-move="2" onClick={this.props.playMove}>Scissors</button></li>
        </ul> :
        <button type="button" className="btn btn-sm btn-green continue-btn" onClick={this.props.startTheRound}>New round</button>
    )
  }

  renderGameScreen = () => {
    return (
      <div>
        <button type="button" className="btn btn-sm btn-red end-game-btn" onClick={this.props.endTheGame}>End the game!</button>
        <div className="container game-wrapper">
          <ContenderContainer contender="player" />
          <ContenderContainer contender="computer" />
        </div>
        {this.renderPlayerButtons()}
      </div>
    )
  }

  renderCelebrationScreen = () => {
    const playerWins = this.props.score.player
    const computerWins = this.props.score.computer
    const draw = playerWins === computerWins ? true : false

    if (draw) {
      return <div className="celebration-text">Nobody win! <span role="img" aria-label="unamused face emoji">😒</span></div>
    } else {
      const winner = playerWins > computerWins ? 'You' : 'Computer'

      return (
        <div>
          <div className="celebration-text">{winner} win!!!</div>
          <img src={celebration} alt="Celebration" className="celebration-img" />
        </div>
      )
    }
  }

  render() {
    return (
      <div className="game-container">
        {!this.props.game.ended ? this.renderGameScreen() : this.renderCelebrationScreen()}
      </div>
    )
  }
}

Game.propTypes = {
  game: PropTypes.shape({
    started: PropTypes.bool,
    ended: PropTypes.bool,
  }),
  roundFinished: PropTypes.bool,
  endTheGame: PropTypes.func,
  startTheRound: PropTypes.func,
  score: PropTypes.shape({
    computer: PropTypes.number,
    none: PropTypes.number,
    player: PropTypes.number,
  }),
}

Game.defaultProps = {
  game: {
    started: true,
    ended: false,
  },
  roundFinished: false,
  endTheGame: () => {},
  startTheRound: () => {},
  score: {
    computer: 0,
    none: 0,
    player: 0,
  },
}
