import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  startTheRound,
  finishTheRound,
  setChoices,
  setScore,
  pushIntoHistory,
  endTheGame,
} from '../../actions'
import { choices } from '../../config'
import Game from './Game'

class GameContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
        playerChoice: {},
        winner: 'none',
    }
  }

  playMove = (e) => {
    const playerMoveNumber = e.target.dataset.move
    const playerMove = choices['player'][playerMoveNumber]
    const computerMove = this.getRandomChoice(choices['computer'])
    const winner = this.whoWins(playerMove.weight, computerMove.weight)
    const roundScore = {
      player: playerMove,
      computer: computerMove,
      winner,
    }

    this.props.finishTheRound()
    this.props.setChoices(playerMove, computerMove)
    this.props.setScore(winner)
    this.props.pushIntoHistory(roundScore)
  }

  getRandomChoice = (choices) => {
    const randomIndex = Math.floor(Math.random() * 3)
    return choices[randomIndex]
  }

  whoWins = (playerChoice, computerChoice) => {
    if (playerChoice % 3 + 1 === computerChoice) {
        return 'computer'
    } else if (computerChoice % 3 + 1 === playerChoice) {
        return 'player'
    }

    return "none"
  }

  render() {
    if (this.props.game.started === false) {
      return (
        <div className="game-container">
          Please, start the game first
          <span role="img" aria-label="face savouring delicious food emoji">😋</span>
        </div>
      )
    }

    return <Game
              game={this.props.game}
              score={this.props.score}
              playMove={this.playMove}
              roundFinished={this.props.roundFinished}
              startTheRound={this.props.startTheRound}
              endTheGame={this.props.endTheGame} />
  }
}

function mapStateToProps (state) {
  return {
    game: state.game,
    roundFinished: state.round.finished,
    score: state.score,
  }
}

export default connect(mapStateToProps, {
  startTheRound,
  finishTheRound,
  setChoices,
  setScore,
  pushIntoHistory,
  endTheGame,
})(GameContainer)