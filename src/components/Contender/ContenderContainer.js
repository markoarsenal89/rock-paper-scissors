import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { choices } from '../../config'
import Contender from './Contender'

class ContenderContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.choices = choices[this.props.contender]
    this.state = { choice: this.choices[0] }
  }

  startLoop = () => {
    let index = 0

    this.loop = setInterval(() => {
      if (index === 3) {
        index = 0
      }

      this.setState({
        choice: this.choices[index++]
      })
    }, 250)
  }

  stopLoop = () => {
    clearInterval(this.loop)
  }

  componentDidMount() {
    if (this.props.round.finished === false) {
      this.startLoop()
    }
  }

  componentWillUnmount() {
    this.stopLoop()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.round.finished === true) {
      this.stopLoop()
      this.setState({
        choice: nextProps.round[this.props.contender]
      })
    } else {
      this.startLoop()
    }
  }

  render() {
    const lastRound = this.props.history[this.props.history.length - 1]
    const contenderWonTheRound = lastRound && lastRound.winner === this.props.contender ? true : false

    return <Contender
              contender={this.props.contender}
              choiceImage={this.state.choice.image}
              contenderScore={this.props.score[this.props.contender]}
              contenderWonTheRound={contenderWonTheRound}
              roundFinished={this.props.round.finished} />
  }
}

ContenderContainer.propTypes = {
  contender: PropTypes.string,
  round: PropTypes.shape({
    finished: PropTypes.bool,
    player: PropTypes.object,
    computer: PropTypes.object,
  }),
  score: PropTypes.shape({
    player: PropTypes.number,
    computer: PropTypes.number,
    none: PropTypes.number,
  }),
  history: PropTypes.array,
}

ContenderContainer.defaultProps = {
  contender: 'computer',
  round: {
    finished: false,
    player: {},
    computer: {},
  },
  score: {
    player: 0,
    computer: 0,
    none: 0,
  },
  history: [],
}

function mapStateToProps (state) {
  return {
    round: state.round,
    score: state.score,
    history: state.history,
  }
}

export default connect(mapStateToProps)(ContenderContainer)
