import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default class Contender extends PureComponent {
  renderWinLoseSign = () => {
    if (this.props.roundFinished === false) {
      return ''
    }

    return (
      (this.props.contenderWonTheRound) ?
      <span className="correct">&#10004;</span> :
      <span className="not-correct">&#10007;</span>
    )
  }

  render() {
    return (
      <div className={`contender ${this.props.contender}`}>
        <h4 className="score">
          {this.props.contender === 'player' ? 'You' : 'Computer'}:
          <span className="score-number"> {this.props.contenderScore} </span>
          {this.renderWinLoseSign()}
        </h4>
        <div className="choice-wrapper">
          <img src={this.props.choiceImage} alt="contender choice" className="contender-choice" />
        </div>
      </div>
    )
  }
}

Contender.propTypes = {
  contender: PropTypes.string,
  choiceImage: PropTypes.string.isRequired,
  score: PropTypes.number,
  contenderWonTheRound: PropTypes.bool,
  roundFinished: PropTypes.bool,
}

Contender.defaultProps = {
  contender: 'computer',
  choiceImage: '',
  score: 0,
  contenderWonTheRound: false,
  roundFinished: false,
}
