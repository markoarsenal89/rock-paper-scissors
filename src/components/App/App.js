import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  NavLink,
} from 'react-router-dom'
import Home from '../Home/Home'
import GameContainer from '../Game/GameContainer'
import ScoreHistory from '../ScoreHistory/ScoreHistory'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="app">
        <header className="app-header">
          <h1 className="app-title">Waste an hour having fun</h1>
        </header>
        <main className="main">
          <Router>
            <div>
              <nav>
                <ul className="nav-list">
                  <li><NavLink exact to="/">Home</NavLink></li>
                  <li><NavLink to="/game-history">Game history</NavLink></li>
                </ul>
              </nav>

              <Route exact path="/" component={Home}/>
              <Route path="/game-history" component={ScoreHistory}/>
              <Route path="/game" component={GameContainer}/>
            </div>
          </Router>
        </main>
      </div>
    )
  }
}

export default App
