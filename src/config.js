import rock from './assets/images/p-rock.png'
import paper from './assets/images/p-paper.png'
import scissors from './assets/images/p-scissors.png'
import crock from './assets/images/c-rock.png'
import cpaper from './assets/images/c-paper.png'
import cscissors from './assets/images/c-scissors.png'

const playerChoices = [{
  choiceName: 'rock',
  image: rock,
  weight: 1,
}, {
  choiceName: 'paper',
  image: paper,
  weight: 2,
}, {
  choiceName: 'scissors',
  image: scissors,
  weight: 3,
}]

const comChoices = [{
  choiceName: 'rock',
  image: crock,
  weight: 1,
}, {
  choiceName: 'paper',
  image: cpaper,
  weight: 2,
}, {
  choiceName: 'scissors',
  image: cscissors,
  weight: 3,
}]

export const choices = {
  player: playerChoices,
  computer: comChoices,
}