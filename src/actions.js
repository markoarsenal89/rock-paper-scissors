export const START_GAME = 'START_GAME'
export const END_GAME = 'END_GAME'
export const START_ROUND = 'START_ROUND'
export const FINISH_ROUND = 'FINISH_ROUND'
export const SET_ROUND_CHOICES = 'SET_ROUND_CHOICES'
export const SET_SCORE = 'SET_SCORE'
export const PUSH_INTO_HISTORY = 'PUSH_INTO_HISTORY'

export function startTheGame () {
  return { type: START_GAME }
}

export function endTheGame () {
  return { type: END_GAME }
}

export function startTheRound () {
  return { type: START_ROUND }
}

export function finishTheRound () {
  return { type: FINISH_ROUND }
}

export function setChoices (playerMove, computerMove) {
  return {
    type: SET_ROUND_CHOICES,
    playerMove,
    computerMove,
  }
}

export function setScore (winner) {
  return {
    type: SET_SCORE,
    winner
  }
}

export function pushIntoHistory (roundScore) {
  return {
    type: PUSH_INTO_HISTORY,
    roundScore
  }
}
