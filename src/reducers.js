import { combineReducers } from 'redux'
import {
  START_GAME,
  END_GAME,
  START_ROUND,
  FINISH_ROUND,
  SET_ROUND_CHOICES,
  SET_SCORE,
  PUSH_INTO_HISTORY,
} from './actions'

const gameState = {
  started: false,
  ended: false,
}

function game (state=gameState, action) {
  switch (action.type) {
    case START_GAME:
      return {
        ...gameState,
        started: true,
      }
    case END_GAME:
      return {
        ...state,
        ended: true,
      }
    default:
      return state
  }
}

const roundState = {
  finished: false,
  player: {},
  computer: {},
}

function round (state=roundState, action) {
  switch (action.type) {
    case START_ROUND:
      return {
        ...state,
        finished: false,
      }
    case FINISH_ROUND:
      return {
        ...state,
        finished: true,
      }
    case SET_ROUND_CHOICES:
      return {
        ...state,
        player: action.playerMove,
        computer: action.computerMove,
      }
    case START_GAME:
      return roundState
    default:
      return state
  }
}

const scoreState = {
  player: 0,
  computer: 0,
  none: 0,
}

function score (state=scoreState, action) {
  switch (action.type) {
    case SET_SCORE:
      return {
        ...state,
        [action.winner]: state[action.winner] + 1,
      }
    case START_GAME:
      return scoreState
    default:
      return state
  }
}

function history (state=[], action) {
  switch (action.type) {
    case PUSH_INTO_HISTORY:
      return [...state, action.roundScore]
    case START_GAME:
      return []
    default:
      return state
  }
}

export default combineReducers({
  game,
  round,
  score,
  history,
})